INSERT INTO users
    (id, email, name, username, password_hash, is_admin)
VALUES
    (   1,
        'admin@sreway.com',
        'Mr. Robot',
        'admin',
        '8cb2237d0679ca88db6464eac60da96345513964',
        true
    );

INSERT INTO course_types
    (id, name, description)
VALUES
    (1, 'mini workshop', 'Мини практикум'),
    (2, 'workshop', 'Практикум');


INSERT INTO courses
    (id, name, description)
VALUES
    (1, 'devops', 'Практикум DevOps'),
    (2, 'kubernetes', 'Практикум Kubernetes'),
    (3, 'linux basic', 'Практикум Linux Basics'),
    (4, 'linux advanced', 'Практикум Linux Advanced'),
    (5, 'docker', 'Практикум Docker'),
    (6, 'nginx', 'Практикум Nginx'),
    (7, 'python', 'Практикум Python');


INSERT INTO available_courses
    (id, course_id, course_type_id)
VALUES
    (1, 1, 2),
    (2, 2, 2),
    (3, 3, 2),
    (4, 4, 2),
    (5, 5, 2),
    (6, 6, 1),
    (7, 7, 1);