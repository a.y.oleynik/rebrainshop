CREATE TABLE users
(
    id serial not null unique,
    email varchar(255) not null unique,
    name varchar(255) not null,
    username varchar(255) not null unique,
    password_hash varchar(255) not null,
    is_admin boolean not null default false
);

CREATE TABLE course_types
(
    id serial not null unique,
    name varchar(255) not null unique,
    description varchar(255)
);

CREATE TABLE courses
(
    id serial not null unique,
    name varchar(255) not null unique,
    description varchar(255)
);

CREATE TABLE available_courses
(
    id serial not null unique,
    course_id int references courses(id) on delete cascade not null,
    course_type_id int references course_types(id) on delete cascade not null
);

CREATE TABLE orders
(
    id serial not null unique,
    user_id int references users(id) on delete cascade not null,
    course_id int references courses(id) on delete cascade not null,
    paid boolean not null default false
);