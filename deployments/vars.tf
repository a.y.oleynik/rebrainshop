variable "do_token" {
  type = string
  description = "DigitalOcean access token."
}

variable "do_region" {
  type = string
  description = "Name of region for DigitalOcean resources."
  default = "nyc1"
}

variable "aws_access_key" {
  type = string
  description = "Specifies an AWS access key associated with an IAM user or role."
}

variable "aws_secret_key" {
  type = string
  description = "Specifies the secret key associated with the access key."
}

variable "aws_region" {
  type = string
  default = "eu-west-1"
  description = "The AWS region."
}

variable "owner_tag" {
  type = string
  description = "User ID tag for created resources."
  default = "oleynik"  
}

variable "dns_zone" {
  type = string
  default = "devops.rebrain.srwx.net"
  description = "DNS zone name."
}

variable "ids_ssh_key" {
  type = list(number)
  description = "List of ID's SSH public keys."
  default = [ 32213896, 32584550 ] // Rebrain public key included
}

variable "ans_inv_file_name" {
  type = string
  default = "hosts.yml"
  description = "Ansible inventory filename."
}

variable "instances" {

  type = map(object({
      name = string
      size = string
      image = string
      tags = list(string)
    }))

  default = {
    "proxy" = {
      name = "proxy"
      image = "ubuntu-20-04-x64"
      size = "s-1vcpu-1gb"
      tags = ["proxy", "bastion", "main_proxy"]
    },

    "api" = {
      name = "api"
      image = "ubuntu-20-04-x64"
      size = "s-1vcpu-1gb"
      tags = ["app"]
    },
    
    "db" = {
      name = "db"
      image = "ubuntu-20-04-x64"
      size = "s-1vcpu-1gb"
      tags = ["db"]
    },
  }

  description = "Map of compute instances."

}

variable "virtual_network" {
  type = object({
    name = string
    ip_range = string
  })

  default = {
    name = "dev-network"
    ip_range = "10.0.0.0/28"
  }

  description = "Private network for DigitalOcean resources."
}


variable "ansible_invetory_variables" {
  type = any
  default = [
    {
      name = "ansible_ssh_user"
      value = "root"
      tags = ["all"]
    },
    {
      name = "ansible_python_interpreter"
      value = "auto"
      tags = ["all"]
    },
    
    {
      name = "ansible_ssh_common_args"
      value = "-o StrictHostKeyChecking=no"
      tags = ["all"]
    },
    {
      name = "ansible_ssh_pipelining"
      value = "true"
      tags = ["all"]
    },

    {
      name = "ansible_ssh_transfer_method"
      value = "piped"
      tags = ["all"]
    },
    {
      name = "ansible_ssh_args"
      value = "-o ControlMaster=auto -o ControlPersist=15m"
      tags = ["all"]
    },
    {
      name = "ansible_ssh_args"
      value = "-o ControlMaster=auto -o ControlPersist=15m -o ForwardAgent=yes"
      tags = ["private_network"]
    },

    {
      name = "ansible_ssh_common_args"
      value = "'-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ProxyCommand=\"ssh -o StrictHostKeyChecking=no -W %h:%p -q {{ ansible_ssh_user }}@{{ groups.bastion | difference(groups.private_network) | random }}\"'"
      tags = ["private_network"]
    }
  ]
}