---
%{ if length(droplets) > 0 ~}
all:
  children:
    ${ main_group }:
      hosts:
%{ for droplet in droplets ~}
        ${ droplet.ipv4_address }:
%{ endfor ~}
%{ if include_private_network ~}
    private_network:
      hosts:
%{ for droplet in droplets ~}
%{ if can(droplet.ipv4_address_private) ~}
        ${ droplet.ipv4_address_private }:
%{ endif ~}
%{ endfor ~}
%{ endif ~}
%{ if length([for item in variables: item if contains(item.tags, "private_network")]) > 0 ~}
      vars:
%{ for variable in variables ~}
%{ if contains(variable.tags, "private_network") ~}
%{ if can(tolist(variable.value)) ~}
      ${ variable.name }:
%{ for item in variable.value ~}
          - ${ item }
%{ endfor ~}
%{ else }        ${ variable.name }: ${ variable.value }
%{ endif ~}
%{ endif ~}
%{ endfor ~}
%{ endif ~}
%{ for tag in tags ~}
%{ if !contains(exlude_tags, tag) ~}
    ${ tag }:
      hosts:
%{ for droplet in droplets ~}
%{ if contains(droplet.tags, tag) ~}
        ${ droplet.ipv4_address }:
%{ if include_private_network && can(droplet.ipv4_address_private) ~}
        ${ droplet.ipv4_address_private }:
%{ endif ~}
%{ endif ~}
%{ endfor ~}
%{ endif ~}
%{ if length([for item in variables: item if contains(item.tags, tag)]) > 0 ~}
      vars:
%{ for variable in variables ~}
%{ if contains(variable.tags, tag) ~}
%{ if can(tolist(variable.value)) ~}
        ${ variable.name }:
%{ for item in variable.value ~}
          - ${ item }
%{ endfor ~}
%{ else }      ${ variable.name }: ${ variable.value }
%{ endif ~}
%{ endif ~}
%{ endfor ~}
%{ endif ~}
%{ endfor ~}
  vars:
%{ for variable in variables ~}
%{ if contains(variable.tags, "all") ~}
%{ if can(tolist(variable.value)) ~}
    ${ variable.name }:
%{ for item in variable.value ~}
      - ${ item }
%{ endfor ~}
%{ else }    ${ variable.name }: ${ variable.value }
%{ endif ~}
%{ endif ~}
%{ endfor ~}
%{ endif ~}
...