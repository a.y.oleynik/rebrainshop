provider "digitalocean" {
  token = var.do_token
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.aws_region
}

resource "digitalocean_tag" "owner_tag" {
  name = var.owner_tag
}

resource "digitalocean_vpc" "private_network" {
  name   = var.virtual_network.name
  region = var.do_region
  ip_range = var.virtual_network.ip_range
}

resource "digitalocean_droplet" "droplets" {
  for_each = var.instances
  name = "${each.value.name}.${var.dns_zone}"
  size = each.value.size
  region = var.do_region
  image = each.value.image
  vpc_uuid = digitalocean_vpc.private_network.id
  ssh_keys = var.ids_ssh_key
  tags = concat([digitalocean_tag.owner_tag.id], each.value.tags)
}

locals {
  bastion_instances = [for item in digitalocean_droplet.droplets : item if contains(item.tags, "bastion")]
  proxy_instances = [for item in digitalocean_droplet.droplets : item if contains(item.tags, "proxy")]
  app_instances = [for item in digitalocean_droplet.droplets : item if contains(item.tags, "app")]
  db_instances = [for item in digitalocean_droplet.droplets : item if contains(item.tags, "db")]
  depends_on = [digitalocean_droplet.droplets]
}

data "aws_route53_zone" "zone" {
  name = var.dns_zone
}

resource "aws_route53_record" "app_dns_records" {
  for_each = {
    // https://github.com/hashicorp/terraform/issues/28925 can't use local.app_instances
    for item in var.instances: item.name => {
      name   = item.name
    } if contains(item.tags, "app")
  }

  zone_id = data.aws_route53_zone.zone.zone_id
  name = "${each.value.name}.${var.owner_tag}.${var.dns_zone}"
  type = "A"
  ttl = 300
  records = local.proxy_instances.*.ipv4_address
  depends_on = [local.proxy_instances, local.app_instances, aws_route53_record.app_dns_records]
}


module "ansible_inventory" {
  source = "git@gitlab.rebrainme.com:devops_users_repos/604/terraform/modules/digitalocean/ansible_inventory.git"
  droplets = digitalocean_droplet.droplets
  exclude_tags = ["oleynik"]
  variables = concat(var.ansible_invetory_variables, [
    {
      name = "app_dns_records"
      value = [for record in aws_route53_record.app_dns_records: record.fqdn]
      tags = ["proxy"]
    },
  ])
  include_private_network = true
  depends_on = [digitalocean_droplet.droplets, aws_route53_record.app_dns_records]
}


resource "local_file" "inventory" {
  content = module.ansible_inventory.data
  filename = "${path.module}/ansible/inventory/${var.ans_inv_file_name}"
  depends_on = [module.ansible_inventory]
}

// Uncomment if need run ansible via terrafrom
/* 
resource "null_resource" "deploy_inventory" {
  triggers = {
    content = local_file.inventory.content
  }
  provisioner "local-exec" {
     working_dir = "${path.module}/ansible"
     command = "ANSIBLE_FORCE_COLOR=1 ansible-playbook -i inventory/${var.ans_inv_file_name} site.yml"
  }
  depends_on = [local_file.inventory]
}
*/