# Deployments

This repository contains Terraform and Ansible configurations for deploy Golang application on DigitalOcean.

## Requirements

- [Terraform](https://www.terraform.io/) (*Used version v1.0.11*)
- [Ansible](https://www.ansible.com/) (*Used version 2.10.9*)

## Dependencies

- [ansible-role-firewall](https://gitlab.rebrainme.com/devops_users_repos/604/ansible/roles/ansible-role-firewall)
- [ansible-role-postgresql](https://gitlab.rebrainme.com/devops_users_repos/604/ansible/roles/ansible-role-postgresql)
- [ansible-role-go](https://gitlab.rebrainme.com/devops_users_repos/604/ansible/roles/ansible-role-go)
- [ansible-role-nginx](https://gitlab.rebrainme.com/devops_users_repos/604/ansible/roles/ansible-role-nginx)
- [nginx-config](https://gitlab.rebrainme.com/devops_users_repos/604/ansible/roles/ansible-role-nginx-config)
- [module_ansible_inventory](https://gitlab.rebrainme.com/devops_users_repos/604/terraform/modules/digitalocean/ansible_inventory)

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.65.0 |
| <a name="provider_digitalocean"></a> [digitalocean](#provider\_digitalocean) | 2.15.0 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.1.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ansible_inventory"></a> [ansible\_inventory](#module\_ansible\_inventory) | https://gitlab.rebrainme.com/devops_users_repos/604/terraform/modules/digitalocean/ansible_inventory | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.app_dns_records](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [digitalocean_droplet.droplets](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet) | resource |
| [digitalocean_tag.owner_tag](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/tag) | resource |
| [digitalocean_vpc.private_network](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/vpc) | resource |
| [local_file.inventory](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [null_resource.deploy_inventory](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [aws_route53_zone.zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ans_inv_file_name"></a> [ans\_inv\_file\_name](#input\_ans\_inv\_file\_name) | Ansible inventory filename. | `string` | `"hosts.yml"` | no |
| <a name="input_ansible_invetory_variables"></a> [ansible\_invetory\_variables](#input\_ansible\_invetory\_variables) | n/a | `any` | <pre>[<br>  {<br>    "name": "ansible_ssh_user",<br>    "tags": [<br>      "all"<br>    ],<br>    "value": "root"<br>  },<br>  {<br>    "name": "ansible_python_interpreter",<br>    "tags": [<br>      "all"<br>    ],<br>    "value": "auto"<br>  },<br>  {<br>    "name": "ansible_ssh_common_args",<br>    "tags": [<br>      "all"<br>    ],<br>    "value": "-o StrictHostKeyChecking=no"<br>  },<br>  {<br>    "name": "ansible_ssh_pipelining",<br>    "tags": [<br>      "all"<br>    ],<br>    "value": "true"<br>  },<br>  {<br>    "name": "ansible_ssh_transfer_method",<br>    "tags": [<br>      "all"<br>    ],<br>    "value": "piped"<br>  },<br>  {<br>    "name": "ansible_ssh_args",<br>    "tags": [<br>      "all"<br>    ],<br>    "value": "-o ControlMaster=auto -o ControlPersist=15m"<br>  },<br>  {<br>    "name": "ansible_ssh_args",<br>    "tags": [<br>      "private_network"<br>    ],<br>    "value": "-o ControlMaster=auto -o ControlPersist=15m -o ForwardAgent=yes"<br>  },<br>  {<br>    "name": "ansible_ssh_common_args",<br>    "tags": [<br>      "private_network"<br>    ],<br>    "value": "'-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ProxyCommand=\"ssh -o StrictHostKeyChecking=no -W %h:%p -q {{ ansible_ssh_user }}@{{ groups.bastion | difference(groups.private_network) | random }}\"'"<br>  }<br>]</pre> | no |
| <a name="input_aws_access_key"></a> [aws\_access\_key](#input\_aws\_access\_key) | Specifies an AWS access key associated with an IAM user or role. | `string` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | The AWS region. | `string` | `"eu-west-1"` | no |
| <a name="input_aws_secret_key"></a> [aws\_secret\_key](#input\_aws\_secret\_key) | Specifies the secret key associated with the access key. | `string` | n/a | yes |
| <a name="input_dns_zone"></a> [dns\_zone](#input\_dns\_zone) | DNS zone name. | `string` | `"devops.rebrain.srwx.net"` | no |
| <a name="input_do_region"></a> [do\_region](#input\_do\_region) | Name of region for DigitalOcean resources. | `string` | `"nyc1"` | no |
| <a name="input_do_token"></a> [do\_token](#input\_do\_token) | DigitalOcean access token. | `string` | n/a | yes |
| <a name="input_ids_ssh_key"></a> [ids\_ssh\_key](#input\_ids\_ssh\_key) | List of ID's SSH public keys. | `list(number)` | <pre>[<br>  32213896,<br>  32584550<br>]</pre> | no |
| <a name="input_instances"></a> [instances](#input\_instances) | Map of compute instances. | <pre>map(object({<br>      name = string<br>      size = string<br>      image = string<br>      tags = list(string)<br>    }))</pre> | <pre>{<br>  "app": {<br>    "image": "ubuntu-20-04-x64",<br>    "name": "app",<br>    "size": "s-1vcpu-1gb",<br>    "tags": [<br>      "app"<br>    ]<br>  },<br>  "db": {<br>    "image": "ubuntu-20-04-x64",<br>    "name": "db",<br>    "size": "s-1vcpu-1gb",<br>    "tags": [<br>      "db"<br>    ]<br>  },<br>  "proxy": {<br>    "image": "ubuntu-20-04-x64",<br>    "name": "proxy",<br>    "size": "s-1vcpu-1gb",<br>    "tags": [<br>      "proxy",<br>      "bastion",<br>      "main_proxy"<br>    ]<br>  }<br>}</pre> | no |
| <a name="input_owner_public_key"></a> [owner\_public\_key](#input\_owner\_public\_key) | SSH public key. | `string` | n/a | yes |
| <a name="input_owner_tag"></a> [owner\_tag](#input\_owner\_tag) | User ID tag for created resources. | `string` | `"oleynik"` | no |
| <a name="input_virtual_network"></a> [virtual\_network](#input\_virtual\_network) | Private network for DigitalOcean resources. | <pre>object({<br>    name = string<br>    ip_range = string<br>  })</pre> | <pre>{<br>  "ip_range": "10.0.0.0/28",<br>  "name": "dev-network"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ansible_inventory"></a> [ansible\_inventory](#output\_ansible\_inventory) | n/a |
<!-- END_TF_DOCS -->

## Variables

- `DEPLOY_GITLAB_TOKEN` - Gitlab Deploy Token ([Usage](https://gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/-/blob/main/deployments/ansible/vars/deploy_app.yml#L2))
- `DEPLOY_GITLAB_USERNAME` - Gitlab Deploy Token ([Usage](https://gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/-/blob/main/deployments/ansible/vars/deploy_app.yml#L3))
- `DEPLOY_DATABASE_PASSWORD` - Password for created database ([Usage](https://gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/-/blob/main/deployments/ansible/vars/deploy_app.yml#L11))

## Usage

1) Clone this repository

```bash
~ git clone git@gitlab.rebrainme.com:devops_users_repos/604/rebrainshop.git
~ cd rebrainshop/deployments
```

2) Install ansible role dependencies

```bash
~ ansible-galaxy install -r ansible/requirements.yml -p ./ansible/roles
```

3) Run terraform and ansible

```bash
~ terraform init
~ terraform plan --var-file=secrets.tfvars
~ terraform apply --var-file=secrets.tfvars
~ ansible-playbook -i inventory/ site.yml
```
