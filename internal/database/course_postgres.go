package database

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"strings"
)

type CoursePostgres struct {
	db *sqlx.DB
}

func NewCoursePostgres(db *sqlx.DB) *CoursePostgres {
	return &CoursePostgres{
		db: db,
	}
}

func (d *CoursePostgres) Create(course models.CreateCourse) (int, error) {
	tx, err := d.db.Begin()
	if err != nil {
		return 0, err
	}
	var id int

	createCourseQuery := fmt.Sprintf("INSERT INTO %s (name, description) VALUES ($1, $2) RETURNING id",
		coursesTable)
	row := tx.QueryRow(createCourseQuery, course.Name, course.Description)

	if err := row.Scan(&id); err != nil {
		tx.Rollback()
		pqErr := PqErrorValidation(err)
		fmt.Println("eee")
		return 0, pqErr
	}

	createCourseListQuery := fmt.Sprintf("INSERT INTO %s (course_id, course_type_id) VALUES ($1, $2)",
		availableCoursesTable)
	_, err = tx.Exec(createCourseListQuery, id, course.TypeId)
	if err != nil {
		tx.Rollback()
		pqErr := PqErrorValidation(err)
		return 0, pqErr
	}

	return id, tx.Commit()
}

func (d *CoursePostgres) Get(courseId int) (models.CreateCourse, error) {
	var course models.CreateCourse

	query := fmt.Sprintf("SELECT c.id, c.name, c.description, ct.id as type_id from %s AS cl INNER JOIN %s AS c ON cl.course_id = c.id INNER JOIN %s AS ct ON cl.course_type_id = ct.id WHERE c.id = $1",
		availableCoursesTable, coursesTable, courseTypeTable)
	err := d.db.Get(&course, query, courseId)

	pqErr := PqErrorValidation(err)

	if pqErr == sql.ErrNoRows {
		return course, fmt.Errorf("does not exist")
	}

	return course, pqErr
}

func (d *CoursePostgres) GetAll() ([]models.ViewCourse, error) {
	var courses []models.ViewCourse
	query := fmt.Sprintf("SELECT c.id, c.name, c.description, ct.name as course_type from %s AS cl INNER JOIN %s AS c ON cl.course_id = c.id INNER JOIN %s AS ct ON cl.course_type_id = ct.id",
		availableCoursesTable, coursesTable, courseTypeTable)
	if err := d.db.Select(&courses, query); err != nil {
		pqErr := PqErrorValidation(err)
		return nil, pqErr
	}

	if courses == nil {
		return nil, fmt.Errorf("does not exist")
	}

	return courses, nil
}

func (d *CoursePostgres) Update(courseId int, updateData models.UpdateCourse) (models.CreateCourse, error)  {
	course, err := d.Get(courseId)

	if err != nil {
		return course, err
	}

	queryCourseItems := make(map[string]interface{})

	var queryCourseSetItems []string

	if updateData.Name != nil && *updateData.Name != course.Name {
		queryCourseItems["name"] = *updateData.Name
	}

	if updateData.Description != nil && *updateData.Description != course.Description {
		queryCourseItems["description"] = *updateData.Description
	}

	for k, v := range queryCourseItems {
		queryCourseSetItems = append(queryCourseSetItems, fmt.Sprintf("%s='%v'", k, v))
	}

	queryCourseSetSection := strings.Join(queryCourseSetItems, ",")

	if queryCourseSetSection == "" {
		return course, fmt.Errorf("nothing update")
	}

	tx, err := d.db.Begin()

	if err != nil {
		return course, err
	}

	query := fmt.Sprintf("UPDATE %s SET %s WHERE id = $1 RETURNING id, name, description",
		coursesTable, queryCourseSetSection)

	row := d.db.QueryRow(query, courseId)

	if err := row.Scan(&course.Id, &course.Name, &course.Description); err != nil {
		tx.Rollback()
		pqErr := PqErrorValidation(err)
		return course, pqErr
	}

	if updateData.TypeId != nil && *updateData.TypeId != course.TypeId {
		query := fmt.Sprintf("UPDATE %s SET course_type_id=$2 WHERE id = $1 RETURNING course_type_id",
			availableCoursesTable)
		row := d.db.QueryRow(query, courseId, *updateData.TypeId)

		if err := row.Scan(&course.TypeId); err != nil {
			tx.Rollback()
			pqErr := PqErrorValidation(err)
			return course, pqErr
		}

	}
	return course, nil
}

func (d *CoursePostgres) Delete(courseId int) error {
	query := fmt.Sprintf("DELETE FROM %s WHERE id = $1", coursesTable)
	res, err := d.db.Exec(query, courseId)
	if err == nil {
		count, err := res.RowsAffected()

		if err == nil && count == 0 {
			return fmt.Errorf("does not exist")
		} else {
			return err
		}
	}

	return err
}
