package database

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"strings"
)

type CourseTypePostgres struct {
	db *sqlx.DB
}

func NewCourseTypePostgres(db *sqlx.DB) *CourseTypePostgres {
	return &CourseTypePostgres{
		db: db,
	}
}


func (d *CourseTypePostgres) Create(courseType models.CourseType) (int, error) {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (name, description) VALUES ($1, $2) RETURNING id", courseTypeTable)
	row := d.db.QueryRow(query, courseType.Name, courseType.Description)
	if err := row.Scan(&id); err != nil {
		pqErr := PqErrorValidation(err)
		return 0, pqErr
	}

	return id, nil
}

func (d *CourseTypePostgres) GetAll() ([]models.CourseType, error) {
	var courseTypes []models.CourseType
	query := fmt.Sprintf("SELECT id, name, description FROM %s", courseTypeTable)
	if err := d.db.Select(&courseTypes, query); err != nil {
		pqErr := PqErrorValidation(err)
		return nil, pqErr
	}

	if courseTypes == nil {
		return nil, fmt.Errorf("does not exist")
	}

	return courseTypes, nil
}

func (d *CourseTypePostgres) Get(courseTypeId int) (models.CourseType, error) {
	var courseType models.CourseType
	query := fmt.Sprintf("SELECT id, name, description FROM %s WHERE id = $1", courseTypeTable)
	err := d.db.Get(&courseType, query, courseTypeId)
	pqErr := PqErrorValidation(err)

	if pqErr == sql.ErrNoRows {
		return courseType, fmt.Errorf("does not exist")
	}

	return courseType, pqErr
}

func (d *CourseTypePostgres) Update(courseTypeId int, updateData models.UpdateCourseType) (models.CourseType, error) {
	courseType, err := d.Get(courseTypeId)

	if err != nil {
		return courseType, err
	}

	queryItems := make(map[string]interface{})
	var querySetItems []string

	if updateData.Name != nil && *updateData.Name != courseType.Name {
		queryItems["name"] = *updateData.Name
	}

	if updateData.Description != nil && *updateData.Description != courseType.Description {
		queryItems["description"] = *updateData.Description
	}

	for k, v := range queryItems {
		querySetItems = append(querySetItems, fmt.Sprintf("%s='%v'", k, v))
	}

	querySetSection := strings.Join(querySetItems, ",")

	if querySetSection == "" {
		return courseType, fmt.Errorf("nothing update")
	}
	query := fmt.Sprintf("UPDATE %s SET %s WHERE id = $1 RETURNING id, name, description",
		courseTypeTable, querySetSection)

	row := d.db.QueryRow(query, courseTypeId)

	if err := row.Scan(&courseType.Id, &courseType.Name, &courseType.Description); err != nil {
		pqErr := PqErrorValidation(err)
		return courseType, pqErr
	}

	return courseType, nil
}


func (d *CourseTypePostgres) Delete(courseTypeId int) error {
	courses, err := d.getCourses(courseTypeId)

	if err != nil {
		return err
	}

	if courses != nil {
		return fmt.Errorf("type used")
	}

	deleteCourseTypesQuery := fmt.Sprintf("DELETE FROM %s WHERE id = $1", courseTypeTable)
	res, err := d.db.Exec(deleteCourseTypesQuery, courseTypeId)
	if err == nil {
		count, err := res.RowsAffected()

		if err == nil && count == 0 {
			return fmt.Errorf("does not exist")
		} else {
			return err
		}
	}

	return err
}

func (d *CourseTypePostgres) getCourses(courseTypeId int) ([]models.CreateCourse, error) {
	var courses []models.CreateCourse
	query := fmt.Sprintf("SELECT c.id, c.name, c.description, cl.course_type_id as type_id FROM %s AS c INNER JOIN %s AS cl ON cl.course_id = c.id WHERE cl.course_type_id = $1",
		coursesTable, availableCoursesTable)
	if err := d.db.Select(&courses, query, courseTypeId); err != nil {
		return nil, err
	}
	return courses, nil
}