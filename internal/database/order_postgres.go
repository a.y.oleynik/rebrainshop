
package database

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
)

type OrderPostgres struct {
	db *sqlx.DB
}

func NewOrderPostgres(db *sqlx.DB) *OrderPostgres {
	return &OrderPostgres{
		db: db,
	}
}

func (d *OrderPostgres) Create(order models.Order, userId int) (int, error) {
	var id int

	createOrderQuery := fmt.Sprintf("INSERT INTO %s (user_id, course_id) VALUES ($1, $2) RETURNING id",
		ordersTable)

	row := d.db.QueryRow(createOrderQuery, userId, order.CourseId)

	if err := row.Scan(&id); err != nil {
		pqErr := PqErrorValidation(err)
		return id, pqErr
	}

	return id, nil
}

func (d *OrderPostgres) Get(orderId int, userId int) (models.ViewOrder, error) {
	var order models.ViewOrder

	query := fmt.Sprintf("SELECT o.id, c.name as course_name, o.paid from %s as o inner join %s as c on o.course_id = c.id where o.id = $1 AND o.user_id = $2",
		ordersTable, coursesTable)
	err := d.db.Get(&order, query, orderId, userId)

	pqErr := PqErrorValidation(err)

	if pqErr == sql.ErrNoRows {
		return order, fmt.Errorf("does not exist")
	}

	return order, pqErr
}

func (d *OrderPostgres) GetAll(userId int) ([]models.ViewOrder, error) {
	var orders []models.ViewOrder
	query := fmt.Sprintf("SELECT o.id, c.name as course_name, o.paid from %s as o inner join %s as c on o.course_id = c.id where o.user_id = $1",
		ordersTable, coursesTable)
	if err := d.db.Select(&orders, query, userId); err != nil {
		pqErr := PqErrorValidation(err)
		return nil, pqErr
	}

	if orders == nil {
		return nil, fmt.Errorf("does not exist")
	}

	return orders, nil
}

func (d *OrderPostgres) Delete(orderId int, userId int) error {
	query := fmt.Sprintf("DELETE FROM %s WHERE id = $1 AND user_id = $2", ordersTable)
	res, err := d.db.Exec(query, orderId, userId)
	if err == nil {
		count, err := res.RowsAffected()

		if err == nil && count == 0 {
			return fmt.Errorf("does not exist")
		} else {
			return err
		}
	}

	return err
}

func (d *OrderPostgres) Pay(orderId int, userId int) error {
	query := fmt.Sprintf("UPDATE %s SET paid = true WHERE id = $1 AND user_id = $2", ordersTable)
	res, err := d.db.Exec(query, orderId, userId)

	if err == nil {
		count, err := res.RowsAffected()

		if err == nil && count == 0 {
			return fmt.Errorf("does not exist")
		} else {
			return err
		}
	}

	return err
}