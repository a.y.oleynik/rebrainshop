package database

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
)

type Config struct {
	Host string
	Port int
	Username string
	Password string
	DBName string
	SSLMode string
}

const (
	usersTable = "users"
	courseTypeTable = "course_types"
	coursesTable = "courses"
	availableCoursesTable = "available_courses"
	ordersTable = "orders"
)

func NewPostgresDB(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf(
		"host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.DBName,cfg.Username, cfg.Password, cfg.SSLMode))

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}

func PqErrorValidation(err error) error {

	pqError, ok := err.(*pq.Error)

	if !ok {
		return err
	} else {
		switch pqError.Code {
		case "23505":
			return fmt.Errorf("already exist")
		case "23503":
			return fmt.Errorf("incorrect input data")
		case "42P01":
			return fmt.Errorf("does not exist")
		default:
			return err
		}
	}
}