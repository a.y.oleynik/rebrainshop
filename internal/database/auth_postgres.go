package database

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
)



type AuthPostgres struct {
	db *sqlx.DB
}

func NewAuthPostgres(db *sqlx.DB) *AuthPostgres {
	return &AuthPostgres{
		db: db,
	}
}

func (d *AuthPostgres) CreateUser(user models.User) (int, error) {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (email, name, username, password_hash, is_admin)" +
		" values ($1, $2, $3, $4, $5) RETURNING id", usersTable)
	row := d.db.QueryRow(query, user.Email, user.Name, user.Username, user.Password, user.IsAdmin)
	if err := row.Scan(&id); err != nil {
		pqErr := PqErrorValidation(err)
		return 0, pqErr
	}
	return id, nil
}


func (d *AuthPostgres) GetUser(username, password string) (models.User, error) {
	var user models.User
	query := fmt.Sprintf("SELECT id, is_admin FROM %s WHERE username=$1 AND password_hash=$2", usersTable)
	err := d.db.Get(&user, query, username, password)
	pqErr := PqErrorValidation(err)

	if pqErr == sql.ErrNoRows {
		return user, fmt.Errorf("does not exist")
	}

	return user, err
}

func (d *AuthPostgres) IsAdminUser(UserId int) (bool, error) {
	var isAdmin bool
	query := fmt.Sprintf("SELECT is_admin FROM %s WHERE id=$1", usersTable)
	err := d.db.Get(&isAdmin, query, UserId)
	return isAdmin, err

}