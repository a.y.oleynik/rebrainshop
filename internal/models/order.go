package models

type Order struct {
	Id int `json:"id"`
	CourseId int `json:"course_id"`
}

type OrderList struct {
	Id int `json:"id"`
	OrderId int `json:"order_id"`
	UserId int `json:"user_id"`
	CourseId int `json:"course_id"`
	Paid bool `json:"paid"`
}

type ViewOrder struct {
	Id int `json:"id" db:"id"`
	CourseName string `json:"course_name" db:"course_name"`
	Paid bool `json:"paid" db:"paid"`
}