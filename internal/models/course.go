package models

type Course struct {
	Id int `json:"id" db:"id"`
	Name string `json:"name" db:"name" binding:"required"`
	Description string `json:"description" db:"description"`
}

type ViewCourse struct {
	*Course
	TypeName string `json:"course_type" db:"course_type"`
}

type CreateCourse struct {
	*Course
	TypeId int `json:"type_id" db:"type_id" binding:"required"`
}

type UpdateCourse struct {
	Name *string `json:"name"`
	Description *string `json:"description"`
	TypeId *int `json:"type_id"`
}


type CourseType struct {
	Id int `json:"id" db:"id"`
	Name string `json:"name" db:"name" binding:"required"`
	Description string `json:"description" db:"description"`
}

type UpdateCourseType struct {
	Name *string `json:"name"`
	Description *string `json:"description"`
}

