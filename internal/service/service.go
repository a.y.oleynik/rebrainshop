package service

import (
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/database"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
)

type Authorization interface {
	CreateUser(user models.User) (int, error)
	GenerateToken(username, password string) (string, error)
	ParseToken(token string) (int, error)
	IsAdminUser(userId int) (bool, error)
}


type Course interface {
	Create(course models.CreateCourse) (int, error)
	Get(courseId int) (models.CreateCourse, error)
	GetAll() ([]models.ViewCourse, error)
	Update(courseId int, updateData models.UpdateCourse) (course models.CreateCourse, err error)
	Delete(courseId int) error
}

type CourseType interface {
	Create(courseType models.CourseType) (int, error)
	Get(courseTypeId int) (models.CourseType, error)
	GetAll() ([]models.CourseType, error)
	Update(courseTypeId int, updateData models.UpdateCourseType) (models.CourseType, error)
	Delete(courseTypeId int) error
}

type Order interface {
	Create(order models.Order, userId int) (int, error)
	Get(orderId int, userId int) (models.ViewOrder, error)
	GetAll(userId int) ([]models.ViewOrder, error)
	Delete(orderId int, userId int) error
	Pay(orderId int, userId int) error
}

type Service struct {
	Authorization
	Course
	CourseType
	Order
}

func NewService(db *database.Database) *Service{
	return &Service{
		Authorization: NewAuthService(db.Authorization),
		Course: NewCourseService(db.Course),
		CourseType: NewCourseTypeService(db.CourseType),
		Order: NewOrderService(db.Order),
	}
}