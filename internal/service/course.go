package service

import (
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/database"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
)

type CourseService struct {
	db database.Course
}

func NewCourseService(db database.Course) *CourseService {
	return &CourseService{
		db: db,
	}
}


func (s *CourseService) Create(course models.CreateCourse) (int, error) {
	return s.db.Create(course)
}

func (s *CourseService) Get(courseId int) (models.CreateCourse, error) {
	return s.db.Get(courseId)
}

func (s *CourseService) Update(courseId int, updateData models.UpdateCourse) (models.CreateCourse, error) {
	return s.db.Update(courseId, updateData)
}

func (s *CourseService) Delete(courseId int) error {
	return s.db.Delete(courseId)
}

func (s *CourseService) GetAll() ([]models.ViewCourse, error) {
	return s.db.GetAll()
}







