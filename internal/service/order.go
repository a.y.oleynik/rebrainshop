package service

import (
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/database"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
)

type OrderService struct {
	db database.Order
}

func NewOrderService(db database.Order) *OrderService {
	return &OrderService{
		db: db,
	}
}

func (s *OrderService) Create(order models.Order, userId int) (int, error) {
	return s.db.Create(order, userId)
}

func (s *OrderService) Get(orderId int, userId int) (models.ViewOrder, error) {
	return s.db.Get(orderId, userId)
}

func (s *OrderService) GetAll(userId int) ([]models.ViewOrder, error) {
	return s.db.GetAll(userId)
}

func (s *OrderService) Delete(orderId int, userId int) error {
	return s.db.Delete(orderId, userId)
}

func (s *OrderService) Pay(orderId int, userId int) error {
	return s.db.Pay(orderId, userId)
}