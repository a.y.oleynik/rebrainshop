package service

import (
	"crypto/sha1"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"os"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/database"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"time"
)

const (
	tokenTTL = 8 * time.Hour
)

type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}


type AuthService struct {
	db database.Authorization
}



func NewAuthService(db database.Authorization) *AuthService  {
	return &AuthService{
		db: db,
	}
}

func (s *AuthService) CreateUser(user models.User) (int, error) {
	user.Password = generatePasswordHash(user.Password)
	return s.db.CreateUser(user)
}

func (s *AuthService) GenerateToken(username, password string) (string, error) {
	user, err := s.db.GetUser(username, generatePasswordHash(password))

	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
		ExpiresAt: time.Now().Add(tokenTTL).Unix(),
		IssuedAt: time.Now().Unix()},
	user.Id,
	})

	signKey := os.Getenv("AUTH_SIGNING_KEY")
	return token.SignedString([]byte(signKey))
}

func (s *AuthService) ParseToken(accessToken string) (int, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("invalid signing method")
		}
		signKey := os.Getenv("AUTH_SIGNING_KEY")
		return []byte(signKey), nil
	})

	if err != nil {
		return 0, fmt.Errorf("invalid token")
	}

	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return 0, fmt.Errorf("invalid token claims")
	}

	return claims.UserId, nil
}

func (s *AuthService) IsAdminUser(userId int) (bool, error) {

	isAdmin, err := s.db.IsAdminUser(userId)

	if err != nil {
		return false, err
	}
	return isAdmin, err
}

func generatePasswordHash(password string) string  {
	hash := sha1.New()
	hash.Write([]byte(password))
	salt := os.Getenv("AUTH_SALT")
	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}

