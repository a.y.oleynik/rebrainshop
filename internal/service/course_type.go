package service

import (
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/database"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
)

type CourseTypeService struct {
	db database.CourseType
}

func NewCourseTypeService(db database.CourseType) *CourseTypeService {
	return &CourseTypeService{
		db: db,
	}
}

func (s *CourseTypeService) Create(courseType models.CourseType) (int, error) {
	return s.db.Create(courseType)
}

func (s *CourseTypeService) GetAll() ([]models.CourseType, error) {
	return s.db.GetAll()
}

func (s *CourseTypeService) Get(courseTypeId int) (models.CourseType, error) {
	return s.db.Get(courseTypeId)
}

func (s *CourseTypeService) Update(courseTypeId int, updateData models.UpdateCourseType) (models.CourseType, error) {
	return s.db.Update(courseTypeId, updateData)
}

func (s *CourseTypeService) Delete(courseTypeId int) (error) {
	return s.db.Delete(courseTypeId)
}
