package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"net/http"
	"net/mail"
)


// @Summary SignUp
// @Tags auth
// @Description create account
// @ID create-account
// @Accept  json
// @Produce  json
// @Param input body models.User true "account info"
// @Success 200 {object} idResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/auth/sign-up [post]
func (h *Handler) signUp(c *gin.Context)  {
	var input models.User
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if _, err := mail.ParseAddress(input.Email); err != nil{
		newErrorResponse(c, http.StatusBadRequest, "incorrect email address")
		return
	}

	id, err := h.services.Authorization.CreateUser(input)


	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

type signInInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}



// @Summary SignIn
// @Tags auth
// @Description login
// @ID login
// @Accept  json
// @Produce  json
// @Param input body signInInput true "credentials"
// @Success 200 {object} tokenResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/auth/sign-in [post]
func (h *Handler) signIn(c *gin.Context) {
	var input signInInput
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	token, err := h.services.Authorization.GenerateToken(input.Username, input.Password)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, tokenResponse{token})
}
