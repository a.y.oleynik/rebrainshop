package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"net/http"
	"strconv"
)

// @Summary Create course
// @Security BearerApiKey
// @Tags courses
// @Description create course </br></br> `only admin user can execute operation`
// @ID create-course
// @Accept  json
// @Produce  json
// @Param input body models.CreateCourse true "course info"
// @Success 200 {object} idResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/ [post]
func (h *Handler) createCourse(c *gin.Context){
	var input models.CreateCourse

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.Course.Create(input)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, idResponse{id})
}

// @Summary Get courses
// @Security BearerApiKey
// @Tags courses
// @Description list of courses
// @ID get-courses
// @Accept  json
// @Produce  json
// @Success 200 {object} []models.ViewCourse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/ [get]
func (h *Handler) getCourses(c *gin.Context) {
	types, err := h.services.Course.GetAll()

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	c.JSON(http.StatusOK, types)
}

// @Summary Get course
// @Security BearerApiKey
// @Tags courses
// @Description get course
// @ID get-course
// @Accept  json
// @Produce  json
// @Param id path int true "Course ID"
// @Success 200 {object} models.CreateCourse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/{id} [get]
func (h *Handler) getCourse(c *gin.Context){
	courseId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid course id param")
		return
	}
	course, err := h.services.Course.Get(courseId)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, course)

}

// @Summary Update course
// @Security BearerApiKey
// @Tags courses
// @Description update course </br></br> `only admin user can execute operation`
// @ID update-course
// @Accept  json
// @Produce  json
// @Param id path int true "Course ID"
// @Param input body models.UpdateCourse true "course info"
// @Success 200 {object} models.CreateCourse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/{id} [put]
func (h *Handler) updateCourse(c *gin.Context) {
	courseId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid course id param")
		return
	}

	var input models.UpdateCourse

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	course, err := h.services.Course.Update(courseId, input)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, course)
}

// @Summary Delete course
// @Security BearerApiKey
// @Tags courses
// @Description delete course </br></br> `only admin user can execute operation`
// @ID delete-course
// @Accept  json
// @Produce  json
// @Param id path int true "Course ID"
// @Success 200 {object} statusResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/{id} [delete]
func (h *Handler) deleteCourse(c *gin.Context)  {
	courseId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid course id param")
		return
	}

	if err := h.services.Course.Delete(courseId); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})
}
