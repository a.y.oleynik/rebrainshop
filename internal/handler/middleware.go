package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

const (
	authorizationHeader = "Authorization"
	userCtx = "userId"
)

func (h *Handler) userIdentity(c *gin.Context)  {
	header := c.GetHeader(authorizationHeader)
	if header == "" {
		newErrorResponse(c, http.StatusUnauthorized, "empty auth header")
		return
	}

	headerParts := strings.Split(header, " ")
	if len(headerParts) != 2 {
		newErrorResponse(c, http.StatusUnauthorized, "invalid auth header")
		return
	}

	userId, err := h.services.Authorization.ParseToken(headerParts[1])

	if err != nil {
		newErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}
	c.Set(userCtx, userId)
}

func (h *Handler) adminRequred (c *gin.Context) {
	id, err := getUserId(c)
	if err != nil {
		return
	}

	isAdmin, err := h.services.Authorization.IsAdminUser(id)

	if err != nil {
		newErrorResponse(c, http.StatusMethodNotAllowed, err.Error())
		return
	}

	if !isAdmin {
		newErrorResponse(c, http.StatusMethodNotAllowed, "permission denided")
		return
	}

	return

}

func getUserId(c *gin.Context)(int, error) {
	userId, ok := c.Get(userCtx)

	if !ok {
		newErrorResponse(c, http.StatusInternalServerError, "user id not found")
		return 0, fmt.Errorf("user id not found")
	}

	id, ok := userId.(int)

	if !ok {
		newErrorResponse(c, http.StatusInternalServerError, "user id is of invalid type")
		return 0, fmt.Errorf("user id is of invalid type")
	}

	return id, nil
}