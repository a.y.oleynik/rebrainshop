package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"net/http"
	"strconv"
)

// @Summary Create order
// @Security BearerApiKey
// @Tags order
// @Description create order
// @ID create-order
// @Accept  json
// @Produce  json
// @Param input body models.Order true "order info"
// @Success 200 {object} idResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/order/ [post]
func (h *Handler) createOrder(c *gin.Context){
	var input models.Order

	userId, err := getUserId(c)
	if err != nil {
		return
	}

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.Order.Create(input, userId)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, idResponse{id})
}

// @Summary Get order
// @Security BearerApiKey
// @Tags order
// @Description get order
// @ID get-order
// @Accept  json
// @Produce  json
// @Param id path int true "Order ID"
// @Success 200 {object} models.ViewOrder
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/order/{id} [get]
func (h *Handler) getOrder(c *gin.Context){
	orderId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid order id param")
		return
	}

	userId, err := getUserId(c)
	if err != nil {
		return
	}

	order, err := h.services.Order.Get(orderId, userId)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, order)
}

// @Summary Get orders
// @Security BearerApiKey
// @Tags order
// @Description get orders
// @ID get-orders
// @Accept  json
// @Produce  json
// @Success 200 {object} []models.ViewOrder
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/order/ [get]
func (h *Handler) getOrders(c *gin.Context){

	userId, err := getUserId(c)
	if err != nil {
		return
	}

	orders, err := h.services.Order.GetAll(userId)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, orders)
}

// @Summary Delete order
// @Security BearerApiKey
// @Tags order
// @Description delete order
// @ID delete-order
// @Accept  json
// @Produce  json
// @Param id path int true "Order ID"
// @Success 200 {object} statusResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/order/{id} [delete]
func (h *Handler) deleteOrder(c *gin.Context)  {
	orderId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid order id param")
		return
	}

	userId, err := getUserId(c)
	if err != nil {
		return
	}

	if err := h.services.Order.Delete(orderId, userId); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})
}

// @Summary Pay order
// @Security BearerApiKey
// @Tags order
// @Description pay order
// @ID pay-order
// @Accept  json
// @Produce  json
// @Param id path int true "Order ID"
// @Success 200 {object} statusResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/order/{id}/pay [post]
func (h *Handler) payOrder(c *gin.Context)  {
	orderId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid order id param")
		return
	}

	userId, err := getUserId(c)
	if err != nil {
		return
	}

	if err := h.services.Order.Pay(orderId, userId); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})
}
