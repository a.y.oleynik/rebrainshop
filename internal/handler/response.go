package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type errorResponse struct {
	Message string `json:"message"`
}

type tokenResponse struct {
	Token string `json:"token"`
}

type statusResponse struct {
	Status string `json:"status"`
}

type idResponse struct {
	Id int `json:"id"`
}

func newErrorResponse(c *gin.Context, statusCode int, message string) {
	logrus.Error(message)
	c.AbortWithStatusJSON(statusCode, errorResponse{message})
}