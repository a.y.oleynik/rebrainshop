package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/models"
	"net/http"
	"strconv"
)


// @Summary Create course type
// @Security BearerApiKey
// @Tags types
// @Description create course type </br></br> `only admin user can execute operation`
// @ID create-course-type
// @Accept  json
// @Produce  json
// @Param input body models.CourseType true "course type info"
// @Success 200 {object} idResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/types/ [post]
func (h *Handler) createCourseType(c *gin.Context){

	_, err := getUserId(c)

	if err != nil {
		return
	}

	var input models.CourseType

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.CourseType.Create(input)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, idResponse{id})
}

// @Summary Get course types
// @Security ApiKeyAuth
// @Tags types
// @Description list types of course
// @ID get-course-types
// @Accept  json
// @Produce  json
// @Success 200 {object} []models.CourseType
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/types/ [get]
func (h *Handler) getCourseTypes(c *gin.Context) {
	types, err := h.services.CourseType.GetAll()

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	c.JSON(http.StatusOK, types)
}

// @Summary Get course type
// @Security ApiKeyAuth
// @Tags types
// @Description get course type
// @ID get-course-type
// @Accept  json
// @Produce  json
// @Param id path int true "Course type ID"
// @Success 200 {object} models.CourseType
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/types/{id} [get]
func (h *Handler) getCourseType(c *gin.Context) {
	courseTypeId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid course type id param")
		return
	}

	courseType, err := h.services.CourseType.Get(courseTypeId)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, courseType)

}

// @Summary Update course type
// @Security ApiKeyAuth
// @Tags types
// @Description update course type </br></br> `only admin user can execute operation`
// @ID update-course-type
// @Accept  json
// @Produce  json
// @Param id path int true "Course type ID"
// @Param input body models.UpdateCourseType true "course type info"
// @Success 200 {object} models.CourseType
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/types/{id} [put]
func (h *Handler) updateCourseType(c *gin.Context) {
	courseTypeId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid course type id param")
		return
	}

	var input models.UpdateCourseType

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	courseType, err := h.services.CourseType.Update(courseTypeId, input)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, courseType)
}

// @Summary Delete course type
// @Security ApiKeyAuth
// @Tags types
// @Description delete course type </br></br> `only admin user can execute operation`
// @ID delete-course-type
// @Accept  json
// @Produce  json
// @Param id path int true "Course type ID"
// @Success 200 {object} statusResponse
// @Failure 400 {object} errorResponse
// @Router /api/v1/courses/types/{id} [delete]
func (h *Handler) deleteCourseType(c *gin.Context)  {
	courseTypeId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid course type id param")
		return
	}

	if err := h.services.CourseType.Delete(courseTypeId); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	c.JSON(http.StatusOK, statusResponse{"ok"})
}
