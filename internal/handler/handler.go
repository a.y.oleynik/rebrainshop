package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	_ "gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/docs"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine  {
	router := gin.New()

	swagger := router.Group("/swagger")
	{
		swagger.GET("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}


	api := router.Group("/api")
	v1 := api.Group("v1")

	auth := v1.Group("/auth")
	{
		auth.POST("/sign-up", h.signUp)
		auth.POST("/sign-in", h.signIn)
	}


	{
		courses := v1.Group("/courses", h.userIdentity)
		{
			courses.GET("/", h.getCourses)
			courses.POST("/", h.adminRequred, h.createCourse)
			courses.GET("/:id", h.getCourse)
			courses.PUT("/:id", h.adminRequred, h.updateCourse)
			courses.DELETE("/:id", h.adminRequred, h.deleteCourse)
			types := courses.Group("/types")
			{
				types.GET("/", h.getCourseTypes)
				types.POST("/",h.adminRequred, h.createCourseType)
				types.GET("/:id", h.getCourseType)
				types.PUT("/:id", h.adminRequred, h.updateCourseType)
				types.DELETE("/:id", h.adminRequred, h.deleteCourseType)
			}

			order := courses.Group("/order")
			{
				order.GET("/", h.getOrders)
				order.POST("/", h.createOrder)
				order.GET("/:id", h.getOrder)
				order.DELETE("/:id", h.deleteOrder)
				order.POST("/:id/pay", h.payOrder)
			}
		}
	}
	return router
}