package main

import (
	"context"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/database"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/handler"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/server"
	"gitlab.rebrainme.com/devops_users_repos/604/rebrainshop/internal/service"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// @title RebrainShop App API
// @version 1.0
// @description API Server for RebrainShop Application,
// @host api.oleynik.devops.rebrain.srwx.net
// @BasePath /
// @securityDefinitions.apikey BearerApiKey
// @in header
// @name Authorization
// @contact.name API Support
// @contact.email andrey.oleynik@sreway.com

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))

	if err := initConfig(); err != nil {
		logrus.Fatalf("init configs error: %s", err.Error())
	}

	pg, err := database.NewPostgresDB(database.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetInt("db.port"),
		Username: viper.GetString("db.username"),
		DBName:   viper.GetString("db.name"),
		SSLMode:  viper.GetString("db.sslmode"),
		Password: viper.GetString("db.password"),
	})


	if err != nil {
		logrus.Fatalf("initialize postgres database error: %s", err.Error())
	}

	db := database.NewDatabase(pg)
	services := service.NewService(db)
	handlers := handler.NewHandler(services)

	srv := new(server.Server)
	go func() {
		if err := srv.Run(viper.GetInt("port"), handlers.InitRoutes()); err != nil {
			logrus.Fatalf("running http server error: %s", err.Error())
		}
	}()

	logrus.Infof("app started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Infof("app shutting down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("shutting down server error: %s", err.Error())
	}

	if err := pg.Close(); err != nil {
		logrus.Errorf("database connection close error: %s", err.Error())
	}
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}