# RebrainShop App API

A RESTful API example for Develepment module

## Installation

1) Get application source code.

```bash
~ git clone gitlab.rebrainme.com/devops_users_repos/604/rebrainshop.git
~ cd rebrainshop 
```

or download via go

```bash
~ go get gitlab.rebrainme.com/devops_users_repos/604/rebrainshop
~ cd $GOPATH/src/gitlab.rebrainme.com/devops_users_repos/604/rebrainshop
```

2) Create application folders.

```bash
~ mkdir -p ~/app/{configs,docs,bin}
```

3) Unsure go-swag installed.

```bash
~ go install github.com/swaggo/swag/cmd/swag@latest
```

4) Update/generate swagger docs.

```bash
swag init -g cmd/rebrainshop/main.go
```

5) Copy swagger docs.

```bash
~ cp docs/*.{json, yaml} ~/app/docs
```

6) Copy configuration and update [config.yml](configs/config.yml) with your values.

```bash
~ cp configs/config.yml ~/app/configs
```

7) Build application.

```bash
~ cd cmd/rebrainshop
~ go build -o ~/app/bin/example-api
```

8) Install go-migrate: [docs](https://github.com/golang-migrate/migrate/tree/master/cmd/migrate)

9) Make database migrations (Change database url with your values).

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' up
```

10) Run application.

```bash
~ cd ~/app
~ ./bin/example-api 
```

## [Database migration usage examples](examples/migrations.md)

## [RESTful API requests examples](examples/requests.md)

## [Deployments](deployments/README.md)

## [Docs](https://api.oleynik.devops.rebrain.srwx.net/swagger/index.html)
