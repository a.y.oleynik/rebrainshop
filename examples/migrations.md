# Database migrations usage examples

- Apply all migrations.

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' up
1/u init (41.497875ms)
2/u set_unique_orders (54.710042ms)
3/u add_data (66.920541ms)
```

- Drop everything inside database.

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' drop -f
```

- Apply rollback migrations.

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' down
Are you sure you want to apply all down migrations? [y/N]
Applying all down migrations
3/d add_data (31.814167ms)
2/d set_unique_orders (51.341708ms)
1/d init (69.238ms)
```

- Apply specific rollback migrations.

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' down 1
```

- Migrate to specific version.

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' goto 2
```

- Example apply all migrations and rolleback without [add_data](schema/000003_add_data.up.sql) migration.

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' up
1/u init (38.678917ms)
2/u set_unique_orders (57.132375ms)
3/u add_data (68.216541ms)
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' down 1
3/d add_data (18.974917ms)
```

or 

```bash
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' up
1/u init (38.678917ms)
2/u set_unique_orders (57.132375ms)
3/u add_data (68.216541ms)
~ migrate -path ./schema -database 'postgres://rebrain:rebrain@localhost:5432/rebrainshop?sslmode=disable' goto 2
3/d add_data (20.485959ms)
```