# RESTful API requests examples

## Create User's Account

Create an Account for the authenticated user if an Account for that User does not already exist. Each User can only have one Account.

**URL** : `api/v1/auth/sign-up`

**Method** : `POST`

**Auth required** : No

**Permissions required** : None

Provide ID of User to be created.

```json
{
    "id": 2
}
```

**Data constraints**

```json
{
    "name": "simple_user",
    "username": "simple_user",
    "email": "a.y.oleynik@gmail.com",
    "password": "12345",
    "is_admin": true
}
```

**CURL**

```bash 
curl --location --request POST 'https://api.oleynik.devops.rebrain.srwx.net/api/v1/auth/sign-up' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "name": "simple_user",
    "username": "simple_user",
    "email": "a.y.oleynik@gmail.com",
    "password": "12345",
    "is_admin": true
}'
```

Notes: if `is_admin` key is `true` user created with admin permissions.

## Login

Used to collect JWT token for a registered user.

**URL** : `api/v1/auth/sign-in`

**Method** : `POST`

**Auth required** : No

**Permissions required** : None

Provide JWT token for a registered user.

```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5..."
}
```

**Data constraints**

```json
{
    "username": "admin",
    "password": "12345"
}
```

**CURL**

```bash
curl --location --request POST 'https://api.oleynik.devops.rebrain.srwx.net/api/v1/auth/sign-in' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "username": "admin",
    "password": "12345"
}'
```

## Get Example Data with JWT token

```bash
curl -s --location --request GET 'https://api.oleynik.devops.rebrain.srwx.net/api/v1/courses/' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Mzg4NDgwODYsImlhdCI6MTYzODgxOTI4NiwidXNlcl9pZCI6MX0.Vk2XYBwa3XskUQddEcIVlJKV5jXXJnqOx9yMqPi2pAc' \
--data-raw '' | jq .
[
  {
    "id": 1,
    "name": "devops",
    "description": "Практикум DevOps",
    "course_type": "workshop"
  },
  {
    "id": 2,
    "name": "kubernetes",
    "description": "Практикум Kubernetes",
    "course_type": "workshop"
  },
  {
    "id": 3,
    "name": "linux basic",
    "description": "Практикум Linux Basics",
    "course_type": "workshop"
  },
  {
    "id": 4,
    "name": "linux advanced",
    "description": "Практикум Linux Advanced",
    "course_type": "workshop"
  },
  {
    "id": 5,
    "name": "docker",
    "description": "Практикум Docker",
    "course_type": "workshop"
  },
  {
    "id": 6,
    "name": "nginx",
    "description": "Практикум Nginx",
    "course_type": "mini workshop"
  },
  {
    "id": 7,
    "name": "python",
    "description": "Практикум Python",
    "course_type": "mini workshop"
  }
]
```

See all [docs](http://api.oleynik.devops.rebrain.srwx.net/swagger/index.html)
```
